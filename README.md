# BigObject Service client

This is the RPC client for BigObject Service powered by Apache Thrift.

- [What is BigObject](http://docs.bigobject.io/)
- [Get BigObject on HEROKU](https://addons.heroku.com/bigobject)

# Basic usage instructions

As of now we have not had a good channel to distribute built binaries for
Apache Thrift and BIgObject Service client.  If you are interested in hosting,
or know an established distribution channel please let us know.

In the mean time, prebuilt binaries can be obtained on a per request basis.
You could try compile this and Apache Thrift C# lib yourself if that interests
you.  This is tested on the Visual Studio Community 2013

* Pre-Built library for [bosrvclient](http://goo.gl/rIvshV)
* Pre-Built library for [Apache Thrift](http://goo.gl/PFtPRP)

bosrvclient utilizes HTTP/HTTPS transport and Apache Thrift JSON protocol for
delivering and recving data.

To create a connection helper, start by including **bosrvclient**

```
using bosrv;
using exc;

Connect conn = Connect.obtain(<uri_to_bigobject>, <timeout>);

BigObjectService.Client client = conn.Client;
String token = conn.Token;
```

A typical query structure can be layout into the following:

- Specify and send your query via execute method
- For methods returning a handle (sha256 string), use cursor helpers to get
  data back
- Data retrieved are encoded as JSON string.

```
String res = client.execute(token, <bigobject_query_stmt>, "", "");

RangeSpec rngspec = new RangeSpec();
rnpspec.page = 100;
int eol = 0;

while (eol != -1)
{
    JArray data = JArray.parse(client.cursor_fetch(
        token,
        res,
        rngspec
    );
    eol = data[0];
    JArray rows = data[1];
}
```

## Race conditions with cursor access

While all operations can be interleaved, *cursor* keep track of access progress
through internal data update.  To avoid race condition, specify *start* to in
**RangeSpec** to take hold of indexing at each *cursor\_fetch*.

# Handling resource references

We strongly urge you to close the returned resource.  While garbage collection
is done routinely on the BigObject service, it intended for unexpected
termination from client application, hence the garbage collection cycle is
kept at a minimal pace.

While it is valid to cache the returned resource handle, make sure your
application handles exceptions accordingly.

# Exceptions produced by API during runtime

## AuthError

produced when providing an invalid authentication token

## ServiceError

general mishaps or signs of really bad server state