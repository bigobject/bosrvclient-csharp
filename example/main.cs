using bosrv;
using Newtonsoft.Json.Linq;
using System;

public class SqlClient
{
    public static void Main()
    {
        try
        {
            Connect conn = Connect.obtain("bo://:@192.168.59.3:8080");

            BigObjectService.Client client = conn.Client;
            String token = conn.Token;

            String result = client.admin_execute(token, "show tables", "", "");
            JArray thing = JArray.Parse(result);
            Console.WriteLine(thing);
        }
        catch (Exception x)
        {
            Console.WriteLine(x.StackTrace);
        }

        Console.Write("\ndemo ended... (continue)");
        Console.Read();
    }
}
