﻿using System;
using Thrift.Protocol;
using Thrift.Transport;

namespace bosrv
{
    public class Connect
    {
        public BigObjectService.Client Client { get; set; }
        public String Token { get; set; }

        public static Connect obtain(String addr_info, int timeout)
        {
            return new Connect(addr_info, timeout);
        }

        public static Connect obtain(String addr_info)
        {
            return new Connect(addr_info, 30);
        }

        private Connect(String addr_info, int timeout)
        {
            UriBuilder uri = new UriBuilder(addr_info);

            String scheme = uri.Scheme.Equals("bos") ? "https" : "http";
            int port = uri.Port;

            UriBuilder cannoical = new UriBuilder(); 
            if (port != -1)
            {
                cannoical.Scheme = scheme;
                cannoical.Host = uri.Host;
                cannoical.Port = uri.Port;
                cannoical.Path = uri.Path;
            }
            else
            {
                cannoical.Scheme = scheme;
                cannoical.Host = uri.Host;
                cannoical.Path = uri.Path;
            }

            Token = uri.Password;

            TProtocol protocol = new TJSONProtocol.Factory().GetProtocol(
                new THttpClient(cannoical.Uri)
            );

            Client = new BigObjectService.Client(protocol);
        }
    }
}
